//
//  SceneType.swift
//  LazyPomodoro
//
//  Created by Maria Saveleva on 05/03/2019.
//  Copyright © 2019 Maria Saveleva. All rights reserved.
//

import Foundation

enum SceneType {
    case timers
    case statistics
    case projects
    case settings
}
