![title](https://github.com/msaveleva/lazy-pomodoro/blob/master/GithubGraphics/Github%20title%202x.png)

Lazy Pomodoro is a productivity iOS app.
It uses a popular idea of work/rest cycle with predefined time intervals. 


## Main features
![presentation](https://github.com/msaveleva/lazy-pomodoro/blob/master/GithubGraphics/Presentation%202x.png)

#### In progress:
- Lazy Pomodoro allows not only set up custom intervals for work and breaks but also provides three pre-defined presets of most popular intervals.
- It is possible to create different projects inside the app, so intervals will be connected with it, which will make calculations of time spend on each task much easier.
- Intervals goals per project and per day. It is always good to track your progress!
- A detailed statistic is available for every project and interval for it.